# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Maintainer: Drew DeVault <sir@cmpwn.com>
pkgname=py3-requests
pkgver=2.30.0
pkgrel=0
pkgdesc="HTTP request library for Python3"
url="https://requests.readthedocs.io/"
arch="noarch"
license="Apache-2.0"
# Requirements for tests are not available in main
options="!check"
depends="
	py3-certifi
	py3-charset-normalizer
	py3-idna
	py3-urllib3
	python3
	"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/r/requests/requests-$pkgver.tar.gz"
builddir="$srcdir/requests-$pkgver"

replaces="py-requests" # Backwards compatibility
provides="py-requests=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
a950a156ac78e5e315b95619f8753f7ba837c63198e256ea69a53f9492f7be1cecb295fcacbf091f3d5ae077ea02c67056d753f99c2b95cc9d233c3ca77f2905  requests-2.30.0.tar.gz
"
