# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kde-inotify-survey
pkgver=23.04.1
pkgrel=0
pkgdesc="Tooling for monitoring inotify limits and informing the user when they have been or about to be reached"
url="https://invent.kde.org/system/kde-inotify-survey"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="BSD-3-Clause AND (GPL-2.0-only OR GPL-3.0-only)"
# zstd is purely used to unpack the source archive
makedepends="
	extra-cmake-modules
	kauth-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kio-dev
	knotifications-dev
	polkit-qt-1-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kde-inotify-survey-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8d6408d5f94ce08f86647a38a2ed2995dc85f416b0e2ec2113a8f7096baa64978ed3b7ba2485b78621d8790fa04de90a181cb4059d7a3282eaae28e26dd22088  kde-inotify-survey-23.04.1.tar.xz
"
