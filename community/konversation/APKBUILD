# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=konversation
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://konversation.kde.org/"
pkgdesc="A user-friendly and fully-featured IRC client"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	karchive-dev
	kbookmarks-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kidletime-dev
	kio-dev
	kitemviews-dev
	knewstuff-dev
	knotifications-dev
	knotifyconfig-dev
	kparts-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	phonon-dev
	qca-dev
	qt5-qtbase-dev
	qt5-qtmultimedia-dev
	samurai
	solid-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/konversation-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f849f0a946ef33b83eee124a72a3f7529ba4be794eafc836e5fcfc6791e9ba47a6bd2572d53d0b3d8b761d4b4fcf24c7c3bdf518d9319ed98d60710f1673cb1e  konversation-23.04.1.tar.xz
"
