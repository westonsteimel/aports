# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=openmp
pkgver=16.0.3
pkgrel=0
_llvmver=${pkgver%%.*}
pkgdesc="LLVM OpenMP Runtime Library"
url="https://openmp.llvm.org/"
arch="all !s390x" # LIBOMP_ARCH = UnknownArchitecture
license="Apache-2.0"
depends_dev="
	$pkgname=$pkgver-r$pkgrel
	"
makedepends="
	clang
	cmake
	elfutils-dev
	libffi-dev
	llvm$_llvmver-dev
	llvm$_llvmver-static
	perl
	samurai
	"
checkdepends="llvm$_llvmver-test-utils"
subpackages="$pkgname-dev"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-${pkgver//_/-}/openmp-${pkgver//_/}.src.tar.xz
	https://github.com/llvm/llvm-project/releases/download/llvmorg-${pkgver//_/-}/cmake-${pkgver//_/}.src.tar.xz
	"
builddir="$srcdir/$pkgname-${pkgver//_/}.src"
options="!check" # todo

case "$CARCH" in
aarch64|ppc64le|x86_64)
	depends_dev="
		$depends_dev 
		$pkgname-bitcode=$pkgver-r$pkgrel
		"
	subpackages="
		$subpackages
		$pkgname-bitcode
		libomptarget
		libomptarget-rtl-cuda
		libomptarget-rtl-cuda-nextgen:cuda_nextgen
		libomptarget-rtl-amdgpu
		libomptarget-rtl-amdgpu-nextgen:amdgpu_nextgen
		libomptarget-rtl-nextgen
		libomptarget-rtl
		"
	;;
riscv64)
	depends_dev="
		$depends_dev 
		$pkgname-bitcode=$pkgver-r$pkgrel
		"
	subpackages="
		$subpackages
		$pkgname-bitcode
		libomptarget
	"
esac

prepare() {
	default_prepare
	mv "$srcdir"/cmake-${pkgver//_/}.src "$srcdir"/cmake
}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CC=clang \
	CXX=clang++ \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DLIBOMP_INSTALL_ALIASES=OFF \
		-DCLANG_TOOL=/usr/bin/clang \
		-DLINK_TOOL=/usr/bin/llvm-link \
		-DOPT_TOOL=/usr/bin/opt \
		-DPACKAGER_TOOL=/usr/bin/clang-offload-packager \
		-DOPENMP_LLVM_TOOLS_DIR=/usr/lib/llvm$_llvmver/bin \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	cmake --build build --target check-openmp
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	rm -f "$pkgdir"/usr/lib/libarcher_static.a
}

libomptarget() {
	amove usr/lib/libomptarget.so.*
}

rtl() {
	amove usr/lib/libomptarget.rtl.*.so.*
}

nextgen() {
	amove usr/lib/libomptarget.rtl.*.nextgen.so.*
}

amdgpu() {
	amove usr/lib/libomptarget.rtl.amdgpu.so.*
}

amdgpu_nextgen() {
	amove usr/lib/libomptarget.rtl.amdgpu.nextgen.so.*
}

cuda_nextgen() {
	amove usr/lib/libomptarget.rtl.cuda.nextgen.so.*
}

cuda() {
	amove usr/lib/libomptarget.rtl.cuda.so.*
}

bitcode() {
	amove usr/lib/libomptarget*.bc
}

sha512sums="
a94e6b31b6a93570bf2b218f08b17187a37100122927be7d9c53efea2fead7dcb026f280daa293017743761e88284a61ec2eded5dddc9cdb584cb7177022c750  openmp-16.0.3.src.tar.xz
01dd624f5276910760f15062f1f3abf68c6ab66c02a4b683ee739663d3f67c47420d8ce47d79e47144edc6bfcc390ffe37bd01d787186607421e3be7288c8569  cmake-16.0.3.src.tar.xz
"
