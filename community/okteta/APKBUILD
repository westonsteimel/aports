# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=okteta
pkgver=0.26.10
pkgrel=0
pkgdesc="KDE hex editor for viewing and editing the raw data of files"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.okteta"
license="(LGPL-2.1-only OR LGPL-3.0-only) AND (GPL-2.0-only OR GPL-3.0-only) AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kbookmarks-dev
	kcmutils-dev
	kcodecs-dev
	kcompletion-dev
	kconfigwidgets-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	kparts-dev
	kservice-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qca-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtscript-dev
	qt5-qttools-dev
	qt5-qtxmlpatterns-dev
	samurai
	shared-mime-info
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/okteta/$pkgver/src/okteta-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_DESKTOPPROGRAM=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -j1
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
746a838cf2fce769fd3660189f070c2a2105f2b2eab7c231dd6be42f2e71518afaef2c77a9d9906847ef1febb46fce66079d485459f6c22545e72feca7f747ea  okteta-0.26.10.tar.xz
"
