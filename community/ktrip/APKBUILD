# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=ktrip
pkgver=23.04.1
pkgrel=0
pkgdesc="A public transport assistant"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by qqc2-desktop-style
arch="all !armhf !s390x !riscv64"
url="https://invent.kde.org/utilities/ktrip"
license="GPL-2.0-only OR GPL-3.0-only"
depends="
	kde-icons
	kirigami-addons
	kirigami2
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcontacts-dev
	ki18n-dev
	kitemmodels-dev
	kpublictransport-dev
	qqc2-desktop-style-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/ktrip-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKF_IGNORE_PLATFORM_CHECK=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
69199efa95b525edcebcf18c33a0620243f9f593f4e6b2924b11bb9f14ab556f6726832e1d46c6b90ee7baca38db51534872f660896b97f627cb1a70fcd35e5e  ktrip-23.04.1.tar.xz
"
