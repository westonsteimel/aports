# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: omni <omni+alpine@hack.org>
pkgname=dune
pkgver=3.7.1
pkgrel=0
pkgdesc="A composable build system for OCaml (formerly Jbuilder)"
url="https://dune.build/"
arch="all !riscv64" # limited by ocaml aport
license="Apache-2.0"
checkdepends="bash"
makedepends="ocaml"
provides="jbuilder=$pkgver-r$pkgrel"
subpackages="$pkgname-doc $pkgname-emacs::noarch $pkgname-configurator"
source="$pkgname-$pkgver.tar.gz::https://github.com/ocaml/dune/archive/$pkgver.tar.gz"
options="!check"  # FIXME requires ocaml-menhir

# 32-bit archs
case "$CARCH" in
	arm*|x86) options="$options textrels" ;;
esac

prepare() {
	default_prepare

	# This allows 'dune --version' to output the correct version instead of "n/a"
	sed -i "/^(name dune)/a (version $pkgver)" dune-project
	# This enables dune-configurator to use the vendored csexp module
	sed -i 's/stdune.csexp/dune-configurator.csexp/' vendor/csexp/src/dune
}

build() {
	./configure --libdir="$(ocamlc -where)" \
		--bindir="/usr/bin" --sbindir="/usr/sbin" --etcdir="/etc" \
		--mandir="/usr/share/man" --docdir="/usr/share/doc" \
		--datadir="/usr/share"

	ocaml boot/bootstrap.ml --verbose
	./dune.exe build \
		-p dune,dune-configurator \
		--profile dune-bootstrap --verbose
}

check() {
	./dune.exe runtest --verbose
}

package() {
	./dune.exe install \
		--destdir="$pkgdir" \
		dune dune-configurator

	# Duplicate of usr/share/doc/dune
	rm -Rf "$pkgdir"/usr/share/doc/dune-configurator
}

emacs() {
	depends="$pkgname"
	install_if="$pkgname=$pkgver-r$pkgrel emacs"
	pkgdesc="Emacs plugins for $pkgname"

	amove usr/share/emacs
}

configurator() {
	pkgdesc="System config helper for the Dune OCaml build system"
	depends="ocaml"
	provides="$subpkgname-dev=$pkgver-r$pkgrel"

	amove usr/lib/ocaml/"$subpkgname"
}

sha512sums="
d741b2a92e970f9747240d4356045f46447238f7c8d8c47ba0b1cad96b8194461a47fa315d86a1eef2033a4d38001c999e6033c31b0c75a311367fb78ea12b6b  dune-3.7.1.tar.gz
"
