# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kopete
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://userbase.kde.org/Kopete"
pkgdesc="An instant messenger supporting AIM, ICQ, Jabber, Gadu-Gadu, Novell GroupWise Messenger, and more"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	gpgme-dev
	libidn-dev
	kcmutils-dev
	kconfig-dev
	kcontacts-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdelibs4support-dev
	kdnssd-dev
	kdoctools-dev
	kemoticons-dev
	khtml-dev
	ki18n-dev
	kidentitymanagement-dev
	kitemmodels-dev
	knotifyconfig-dev
	kparts-dev
	ktexteditor-dev
	kwallet-dev
	libkleo-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kopete-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
b3d3ea69417c203e7b77ea3c94f664de73378267c0352420d5b8c4ba5f5b9ebebe91bde3b313692d335fc5b49de676948f89c344fcc23bc836321ece6ab11d9f  kopete-23.04.1.tar.xz
"
