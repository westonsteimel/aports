# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=itinerary
pkgver=23.04.1
pkgrel=0
# armhf blocked by qt5-qtdeclarative
# ppc64le blocked by kitinerary
# s390x blocked by qt5-qtdeclarative
arch="aarch64 armv7 x86_64 x86"
url="https://invent.kde.org/pim/itinerary"
pkgdesc="Itinerary and boarding pass management application"
license="BSD-3-Clause AND LGPL-2.0-or-later"
depends="
	kirigami-addons
	kirigami2
	kitemmodels
	kopeninghours
	prison
	qt5-qtlocation
	tzdata
	"
makedepends="
	extra-cmake-modules
	kcontacts-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kholidays-dev
	ki18n-dev
	kirigami-addons-dev
	kitinerary-dev
	knotifications-dev
	kosmindoormap-dev
	kpkpass-dev
	kpublictransport-dev
	networkmanager-qt-dev
	qqc2-desktop-style-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtlocation-dev
	qt5-qtquickcontrols2-dev
	samurai
	shared-mime-info
	solid-dev
	zlib-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/itinerary-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# tripgrouptest and timelinemodel test are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "(tripgroup|timelinemodel)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
65f680ce6390a40aae8e31a46d7076d43bd8a59a094af8dd27e70c3e979c0d44b248d0c92d075478bc4373a26ffeb41bc85f81f704f5aabd54a3a1a767d301ac  itinerary-23.04.1.tar.xz
"
