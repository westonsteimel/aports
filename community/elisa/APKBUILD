# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=elisa
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x blocked by vlc
arch="all !armhf !s390x"
url="https://kde.org/applications/multimedia/org.kde.elisa"
pkgdesc="A simple music player aiming to provide a nice experience for its users"
license="LGPL-3.0-or-later"
depends="
	kirigami2
	vlc
	"
makedepends="
	baloo-dev
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdeclarative-dev
	kdoctools-dev
	kfilemetadata-dev
	ki18n-dev
	kio-dev
	kirigami2-dev
	kpackage-dev
	kxmlgui-dev
	samurai
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	vlc-dev
	"
checkdepends="
	cmd:dbus-run-session
	xvfb-run
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/elisa-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# mediaplaylistproxymodelTest, elisaqmltests and localfilelistingtest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE dbus-run-session xvfb-run ctest -E "(viewManagerTest|mediaplaylistproxymodelTest|elisaqmltests|localfilelistingtest)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
fe6d09255d6d594605f77e7ce464909b7bf266e557df8abb2cb06c75d24f2c79d45dc4ace3e6513484ad1883fa1888355ad785932a2d1746ea9bf42aeb7f80b6  elisa-23.04.1.tar.xz
"
