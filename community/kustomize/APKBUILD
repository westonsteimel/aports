# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=kustomize
pkgver=5.0.2
pkgrel=0
pkgdesc="Template-free customization of Kubernetes YAML manifests"
url="https://kustomize.io/"
license="Apache-2.0"
arch="all"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/kubernetes-sigs/kustomize/archive/kustomize/v$pkgver/kustomize-$pkgver.tar.gz"
builddir="$srcdir/kustomize-kustomize-v$pkgver/kustomize"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local builddate="$(date -u "+%Y-%m-%dT%TZ" ${SOURCE_DATE_EPOCH:+-d @$SOURCE_DATE_EPOCH})"

	go build -ldflags="
		-X sigs.k8s.io/kustomize/api/provenance.version=$pkgver
		-X sigs.k8s.io/kustomize/api/provenance.buildDate=$builddate
		"

	./kustomize completion bash > $pkgname.bash
	./kustomize completion fish > $pkgname.fish
	./kustomize completion zsh > $pkgname.zsh
}

check() {
	go test ./...
}

package() {
	install -Dm755 kustomize -t "$pkgdir"/usr/bin/

	install -Dm644 $pkgname.bash "$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish "$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh "$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
37f080b5d514b6ca43a529295046ed8b8921e7657322e940511272e381728d07870a84b77242a5e59457bc21e3af527e6a815b676a2f0a59d752d360e2a8478d  kustomize-5.0.2.tar.gz
"
