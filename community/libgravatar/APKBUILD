# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libgravatar
pkgver=23.04.1
pkgrel=0
pkgdesc="KDE PIM library providing Gravatar support"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> pimcommon
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="
	kconfig-dev
	ki18n-dev
	kio-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	pimcommon-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/libgravatar-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="net" # net required for tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
4ca3440943fc270d1f70cd7a8a2d629ce5d2c6d6e4bda1a0c0a5e6d4414daace8864c037ccda954a6c4f9fdceaa3ab8864d8019bdd81167a439a21071b37e1de  libgravatar-23.04.1.tar.xz
"
