# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=akonadi-import-wizard
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by kmailtransport -> libkgapi -> qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Import data from other mail clients to KMail"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-dev>=$pkgver
	extra-cmake-modules
	kauth-dev
	kconfig-dev
	kcontacts-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kidentitymanagement-dev
	kio-dev
	kmailtransport-dev
	kwallet-dev
	libkdepim-dev
	mailcommon-dev
	messagelib-dev
	pimcommon-dev
	qt5-qtbase-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-import-wizard-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f7083a5b4d01f35a0a91ee9738a03a8aca7645490263f80435bb2a758a1ffe604822eacf43c6e3ce0b75c728a81642710365248fec3daf406baa90e03211f574  akonadi-import-wizard-23.04.1.tar.xz
"
