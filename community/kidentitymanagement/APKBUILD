# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kidentitymanagement
pkgver=23.04.1
pkgrel=0
pkgdesc="KDE PIM libraries"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org"
license="LGPL-2.0-or-later"
depends_dev="
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kiconthemes-dev
	kio-dev
	kpimtextedit-dev
	ktextwidgets-dev
	kxmlgui-dev
	qt5-qtbase-dev
	"
makedepends="
	$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/kidentitymanagement-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kpimidentity-signaturetest is broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kpimidentity-signaturetest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
53ce7201ac9714141bf47a6dd26d909d22cc646528474cecdacab8a2e8cf04d0dbd80bfdeab3e9394e123c5f912a63ba07204e22f398a12a6ce1d52de167d15e  kidentitymanagement-23.04.1.tar.xz
"
